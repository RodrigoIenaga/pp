package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/cs")
public class ControleSessao extends HttpServlet {

	private int contador;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		HttpSession session = req.getSession();
		
		req.setAttribute("sessao", session);
		req.setAttribute("contador", contador);
		
		RequestDispatcher dispacther = req.getRequestDispatcher("ControleSessao.jsp");
		dispacther.forward(req, resp);
		

	}
}
 