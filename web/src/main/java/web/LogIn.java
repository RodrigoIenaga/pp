package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/LogIn")
public class LogIn extends HttpServlet{
		
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			PrintWriter out = resp.getWriter();
			
			String login = req.getParameter("usuario");
			String senha = req.getParameter("senha");
			HttpSession session = req.getSession();

			if(login.equals("admin") && senha.equals("1234")) {		
				
				RequestDispatcher dispacther = req.getRequestDispatcher("ciclo");
				dispacther.forward(req, resp);	
				
				session.setMaxInactiveInterval(10);
				
			}
			else {
				out.println(session.getId());
				
				String url = "localhost:8080/web";
				resp.sendRedirect(url);
				session.invalidate();
				
			}	
		}
}
