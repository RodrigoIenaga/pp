package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/primeiro")
public class Primeiro extends HttpServlet {
	
	private int contador;
	
	@Override
	public void init() throws ServletException{
		System.out.println("Servlet Iniciado.");
		System.out.println("Contador inicial: " + contador);
	}	
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		String login = req.getParameter("usuario");
		String senha = req.getParameter("senha");
		HttpSession session = req.getSession();

		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		if(login.equals("admin") && senha.equals("1234")) {
			out.println("<h3>Seja bem vindo, " + login + "</h3>");
			out.println(session.getId());
			
			RequestDispatcher dispacther = req.getRequestDispatcher("cv");
			
			dispacther.forward(req, resp);
			
			session.setMaxInactiveInterval(10);
			
		}
		else {
			out.println("<h3>usuario ou senha incorreto</h3>");
			session.invalidate();
			out.println(session.getId());
			
			String url = "";
			resp.sendRedirect(url);

		}
		out.println("</body>");
		out.println("</html>");	
		
		contador = contador + 1;
		out.println("Numero de tentativa de logIns: " + contador);
	}
	
	@Override
	public void destroy() {
		System.out.println("Servlet destruido.");
		System.out.println("Contador final: " + contador);
	}
}

